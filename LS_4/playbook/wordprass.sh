#!/usr/bin/env bash

ansible-playbook wordpress.yml \
		-c local \
		"$@"
